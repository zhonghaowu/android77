package com.example.android77.chess;

import com.example.android77.R;
import com.example.android77.piece.Piece;

public class SharedStorage {
    public static String info;
    public static Piece[][] chessBoard;
    public static boolean hasUpdate = false;
    public static boolean shouldProcess = false;
    public static String moveString;
    public static Integer[] gridViewData;
    public static int[] random_start;
    public static int[] random_end;
    public static void updateImage(Piece[][] board){
        Integer[] newImages = new Integer[64];
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                if(board[i][j] == null){
                    newImages[i*8+j] = R.drawable.empty;
                }else if (board[i][j].type.equals("king") && board[i][j].color.equals("white")){
                    newImages[i*8+j] = R.drawable.whitequeen;
                }else if (board[i][j].type.equals("queen") && board[i][j].color.equals("white")){
                    newImages[i*8+j] = R.drawable.whiteking;
                }else if (board[i][j].type.equals("bishop") && board[i][j].color.equals("white")){
                    newImages[i*8+j] = R.drawable.whitebishop;
                }else if (board[i][j].type.equals("rook") && board[i][j].color.equals("white")){
                    newImages[i*8+j] = R.drawable.whiterook;
                }else if (board[i][j].type.equals("pawn") && board[i][j].color.equals("white")){
                    newImages[i*8+j] = R.drawable.whitepawn;
                }else if (board[i][j].type.equals("knight") && board[i][j].color.equals("white")){
                    newImages[i*8+j] = R.drawable.whiteknight;
                }else if (board[i][j].type.equals("king") && board[i][j].color.equals("black")){
                    newImages[i*8+j] = R.drawable.blackqueen;
                }else if (board[i][j].type.equals("queen") && board[i][j].color.equals("black")){
                    newImages[i*8+j] = R.drawable.blackking;
                }else if (board[i][j].type.equals("bishop") && board[i][j].color.equals("black")){
                    newImages[i*8+j] = R.drawable.blackbishop;
                }else if (board[i][j].type.equals("rook") && board[i][j].color.equals("black")){
                    newImages[i*8+j] = R.drawable.blackrook;
                }else if (board[i][j].type.equals("pawn") && board[i][j].color.equals("black")){
                    newImages[i*8+j] = R.drawable.blackpawn;
                }else if (board[i][j].type.equals("knight") && board[i][j].color.equals("black")){
                    newImages[i*8+j] = R.drawable.blackknight;
                }
            }
        }
        gridViewData = newImages;
    }

    public static int highlightPos;
}
