package com.example.android77.chess;

import java.util.ArrayList;
import java.util.Date;

/**
 * Record class
 * hold win, way, title, date and moveRecord.
 * @author Zhonghao Wu, Thomas Cooke
 *
 */
public class Record {

    /**
     * information of the record
     */
    private String win;
    private String way;
    private String title;
    private Date date;
    private ArrayList<int[][]> moveRecord;

    /**
     * @param win winner
     * @param way how did the game end
     * @param title title of the record
     * @param date date of the record
     * @param moveRecord move steps of the record
     */
    public Record (String win, String way, String title, Date date, ArrayList<int[][]> moveRecord) {
        this.win = win;
        this.way = way;
        this.title = title;
        this.date = date;
        this.moveRecord = moveRecord;
    }

    /**
     * get the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * get the move steps
     */
    public ArrayList<int[][]> getMoveRecord() {
        return moveRecord;
    }

    /**
     * get the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * get the winner
     */
    public String getWin() {
        return win;
    }

    /**
     * get the way of the game ended
     */
    public String getWay() {
        return way;
    }

    public void setMoveRecord(ArrayList<int[][]> move){
        this.moveRecord = move;
    }
}
