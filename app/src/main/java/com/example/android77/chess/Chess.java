package com.example.android77.chess;

import android.util.Log;

import com.example.android77.piece.Bishop;
import com.example.android77.piece.King;
import com.example.android77.piece.Knight;
import com.example.android77.piece.Pawn;
import com.example.android77.piece.Piece;
import com.example.android77.piece.Queen;
import com.example.android77.piece.Rook;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Chess {
    /**
     * player who should move
     */
    public Player inAction;
    public Player playerWhite;
    public Player playerBlack;
    /**
     * counter for checking draw requirement
     */
    int globalCounter = 0;
    /**
     * representing draw the time a player offering draw
     */
    private int under_draw_require = 0;
    /**
     * no parameter is needed for this class
     */
    public Board gameBoard;
    public Chess lastChess;

    public Chess deepCopyChess(Chess chess){
        Chess copy = new Chess();
        copy.playerWhite = new Player("white");
        copy.playerBlack = new Player("black");
        copy.playerWhite.another = copy.playerBlack;
        copy.playerBlack.another = copy.playerWhite;
        copy.globalCounter = chess.globalCounter;
        copy.under_draw_require = chess.under_draw_require;
        copy.gameBoard = chess.gameBoard.superCopy(copy.playerWhite, copy.playerBlack);
        if(chess.inAction.color.equals("white"))    copy.inAction = copy.playerWhite;
        else    copy.inAction = copy.playerBlack;
        return copy;
    }
    public Chess() {
        Player pw = new Player("white");
        Player pb = new Player("black");
        pw.another = pb;
        pb.another = pw;
        playerWhite = pw;
        playerBlack = pb;
        this.gameBoard = new Board();
        init(pw, pb, gameBoard);
        this.inAction = pw;
        SharedStorage.chessBoard = gameBoard.board;
        SharedStorage.updateImage(gameBoard.board);
    }

    public String move(int[] start, int[] end, String command) {
        Log.d("move", Arrays.toString(start) + " " + Arrays.toString(end));
        Chess temp = deepCopyChess(this);
        String retVal = "valid";
        if (gameBoard.board[start[0]][start[1]] == null){
            Log.d("invalidLoc","1");
            return "invalid";
        }
        for (Piece p : this.inAction.getPieces(gameBoard))
            if (p.type.equals("pawn") && p.owner == this.inAction) ((Pawn) p).pawnCounter++;
        this.globalCounter++;
        Piece p = gameBoard.board[start[0]][start[1]];
        if (command.equals("queen") || command.equals("bishop") || command.equals("rook") || command.equals("queen")) {
            if (promotion(p, gameBoard, end, command)){
                this.lastChess = temp;
                return "promoted";
            } else{
                this.lastChess = temp;
                Log.d("invalidLoc","2");
                return "invalid";
            }
        }
        if (p.owner != this.inAction){
            Log.d("invalidLoc","3");
            return "invalid";
        }
        if (command.equals("normal") || command.equals("draw?")) {
            if (p.move(gameBoard, end)) {
                //move success
                p.counter++;
                promotion(p,gameBoard,end,command);
                //promotion(p, gameBoard, end, command);
            } else{
                Log.d("invalidLoc","4");
                SharedStorage.chessBoard = this.gameBoard.board;
                SharedStorage.updateImage(gameBoard.board);
                return "invalid";
            }
        }
        String checkmateRes = checkmate_detection(this.inAction.another,gameBoard);
        if(!checkmateRes.equals("no_checkmate")){
            this.lastChess = temp;
            return checkmateRes;
        }
        if(check_detection(this.inAction.another, gameBoard))	retVal = "check";
        if(this.inAction == playerBlack)	this.inAction = playerWhite;
        else	this.inAction = playerBlack;
        SharedStorage.chessBoard = this.gameBoard.board;
        SharedStorage.updateImage(gameBoard.board);
        this.lastChess = temp;
        return retVal;
    }

    public static boolean promotion(Piece p, Board board, int[] end, String type) {
        if(!p.type.equals("pawn"))	return false;
        if((p.color.equals("white") && end[0] == 0) || (p.color.equals("black") && end[0] == 7)) {
            board.board[p.loc[0]][p.loc[1]] = null;
            if(type.equals("bishop")) {
                board.board[end[0]][end[1]] = new Bishop(p.color, p.owner, end, "bishop");
            }else if(type.equals("knight")) {
                board.board[end[0]][end[1]] = new Knight(p.color, p.owner, end, "knight");
            }else if(type.equals("rook")) {
                board.board[end[0]][end[1]] = new Rook(p.color, p.owner, end, "rook");
            }else {
                board.board[end[0]][end[1]] = new Queen(p.color, p.owner, end, "queen");
            }
            return true;
        }
        return false;
    }

    /**
     * distribute pieces to each player
     * @param pw
     * @param pb
     * @param board
     */
    private static void init(Player pw, Player pb, Board board) {
        //distribute pawns
        for(int i = 0; i < 8; i++) {
            Pawn newPawn = new Pawn("white",  pw, new int[] {6, i}, "pawn");
            //pw.pieces.add(newPawn);
            board.board[6][i] = newPawn;
        }
        for(int i = 0; i < 8; i++) {
            Pawn newPawn = new Pawn("black",  pb, new int[] {1, i}, "pawn");
            //pb.pieces.add(newPawn);
            board.board[1][i] = newPawn;
        }
        //distribute kings
        King newKing = new King("white", pw, new int[] {7,4}, "king");
        King newKing2 = new King("black", pb, new int[] {0,4}, "king");
        board.board[7][4] = newKing;
        board.board[0][4] = newKing2;
        //distribuet queens
        board.board[7][3] = new Queen("white", pw, new int[] {7,3}, "queen");
        board.board[0][3] = new Queen("black", pb, new int[] {0,3}, "queen");
        //distribuet bishops
        board.board[7][2] = new Bishop("white", pw, new int[] {7,2}, "bishop");
        board.board[7][5] = new Bishop("white", pw, new int[] {7,5}, "bishop");
        board.board[0][2] = new Bishop("black", pb, new int[] {0,2}, "bishop");
        board.board[0][5] = new Bishop("black", pb, new int[] {0,5}, "bishop");
        //distribuet knights
        board.board[7][1] = new Knight("white", pw, new int[] {7,1}, "knight");
        board.board[7][6] = new Knight("white", pw, new int[] {7,6}, "knight");
        board.board[0][1] = new Knight("black", pb, new int[] {0,1}, "knight");
        board.board[0][6] = new Knight("black", pb, new int[] {0,6}, "knight");
        //distribuete rook
        board.board[7][0] = new Rook("white", pw, new int[] {7,0}, "rook");
        board.board[7][7] = new Rook("white", pw, new int[] {7,7}, "rook");
        board.board[0][0] = new Rook("black", pb, new int[] {0,0}, "rook");
        board.board[0][7] = new Rook("black", pb, new int[] {0,7}, "rook");


    }


    /**
     *
     * @param pw
     * @param pb
     * @param board
     */
    private static void test_init(Player pw, Player pb, Board board) {

        King newKing = new King("white", pw, new int[] {6,5}, "king");
        King newKing2 = new King("black", pb, new int[] {0,7}, "king");
        board.board[6][5] = newKing;
        board.board[0][7] = newKing2;
        board.board[7][2] = new Bishop("white", pw, new int[] {7,2}, "bishop");
        board.board[5][0] = new Bishop("white", pw, new int[] {5,0}, "bishop");
        board.board[1][0] = new Pawn("black", pb, new int[] {1,0}, "pawn");
        board.board[2][1] = new Pawn("black", pb, new int[] {2,1}, "pawn");
        board.board[1][7] = new Pawn("black", pb, new int[] {1,7}, "pawn");

    }

    /**
     * get the king piece of Player p
     * @param p in action player
     * @param board chess board
     * @return king king of the player
     */
    public static King getKing(Player p, Board board) {
        ArrayList<Piece> all = p.getPieces(board);
        for(Piece current : all)	if(current.type.equals("king"))	return (King)current;
        return null;
    }

    /**
     * check if player p is on check
     * @param p
     * @param board
     * @return true if and only if Player p is on check
     */
    private static boolean check_detection(Player p, Board board) {
        King k = getKing(p, board);
        return k.check_detection(board, k.loc);
    }

    /**
     * check if player p is on checkmate, if yes, print result and shut the game
     * @param p
     * @param board
     */
    private static String checkmate_detection(Player p, Board board) {
        if(!check_detection(p,board))	return "no_checkmate";
        King k = getKing(p,board);
        boolean[][] lm = k.legalMove(board);
        boolean flag = false;
        //king move
        for(int i = k.loc[0] - 1; i <= k.loc[0] + 1; i++) {
            for(int j = k.loc[1] - 1; j <= k.loc[1] + 1; j++) {
                if(Tools.inMatrix(new int[] {i,j}) && lm[i][j]) {
                    if(k.check_detection(board, new int[] {i,j}))	continue;
                    else {
                        flag = true;
                        break;
                    }
                }
            }
        }
        //protector move

        for(Piece protector : p.getPieces(board)) {
            boolean[][] legal_move = protector.legalMove(board);
            int[] temp = protector.loc;
            for(int i = 0; i < 8; i++) {
                for(int j = 0; j < 8; j++) {
                    Piece temp2 = board.get(new int[] {i,j});
                    if(!legal_move[i][j])	continue;
                    board.board[temp[0]][temp[1]] = null;
                    board.board[i][j] = protector;
                    protector.loc = new int[] {i,j};
                    if(!check_detection(p, board)) flag = true;
                    board.board[temp[0]][temp[1]] = protector;
                    board.board[i][j] = temp2;
                    protector.loc = temp;
                }
            }
        }
        if(!flag) {
            if(p.color.equals("white"))	return "checkmate_black_win";
            else System.out.println("checkmate_white_win");
        }
        return "no_checkmate";
    }

    public void magicChange(Piece piece, String type){
        Piece newPiece;
        if (type.equals("bishop")){
            newPiece = new Bishop(piece.color, piece.owner, piece.loc, "bishop");
        }else if(type.equals("rook")){
            newPiece = new Rook(piece.color, piece.owner, piece.loc, "rook");
        }else if(type.equals("knight")){
            newPiece = new Knight(piece.color, piece.owner, piece.loc, "knight");
        }else{
            newPiece = new Queen(piece.color, piece.owner, piece.loc, "queen");
        }
        gameBoard.board[piece.loc[0]][piece.loc[1]] = newPiece;
        SharedStorage.chessBoard[piece.loc[0]][piece.loc[1]] = newPiece;
        SharedStorage.updateImage(gameBoard.board);
    }

    public String randomMove(){
        ArrayList<Piece> all = inAction.getPieces(this.gameBoard);
        Collections.shuffle(all);
        for(Piece piece : all){
            ArrayList<int[]> temp = new ArrayList<>();
            boolean[][] valid = piece.legalMove(this.gameBoard);
            for(int i = 0; i < 8; i++){
                for(int j = 0; j < 8; j++){
                    if(valid[i][j]){
                        temp.add(new int[]{i,j});
                    }
                }
            }
            while(!temp.isEmpty()){
                int[] target = temp.get((int)(Math.random() * temp.size()));
                int[] temp2 = Arrays.copyOf(piece.loc,2);
                String feedback = move(piece.loc, target, "normal");
                if(!feedback.equals("invalid")){
                    SharedStorage.random_start = temp2;
                    SharedStorage.random_end = Arrays.copyOf(target,2);
                    return "valid";
                }else{
                    temp.remove(target);
                }
            }
        }
        return "stalemate";
    }
}
