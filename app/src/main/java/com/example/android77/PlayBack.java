package com.example.android77;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android77.R;
import com.example.android77.chess.Chess;
import com.example.android77.chess.Record;
import com.example.android77.piece.Piece;
import com.google.gson.Gson;

public class PlayBack extends AppCompatActivity {
    TextView textViewBlack;
    TextView textViewWhite;
    Button nextButton;
    int position;
    String win;
    String way;
    Chess chess;
    final String[] items = {"queen", "rook", "bishop", "knight"};
    int choice = -1;
    int currentStepCounter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_play_back);

        Bundle bundle = getIntent().getExtras();
        chess = new Chess();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        nextButton = findViewById(R.id.nextButton);
        textViewBlack = findViewById(R.id.textViewPlayBack1);
        textViewWhite = findViewById(R.id.textViewPlayBack2);

        GridView gridView = (GridView)findViewById(R.id.gridViewPlayBack);
        ImageAdapter myImgAdapter = new ImageAdapter(this);
        gridView.setAdapter(myImgAdapter);



        if (bundle != null) {
            position = bundle.getInt("position");
            win = bundle.getString("win");
            way = bundle.getString("way");
        }
        Record record = MainActivity.getRecords().get(position);


        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (currentStepCounter >= 0 && currentStepCounter < record.getMoveRecord().size()) {

                    int[] start = record.getMoveRecord().get(currentStepCounter)[0];

                    int[] end = record.getMoveRecord().get(currentStepCounter)[1];
                    try {
                        choice = record.getMoveRecord().get(currentStepCounter)[2][0];
                    } catch (ArrayIndexOutOfBoundsException e) {
                        // do nothing;
                    }
                    showStep(start, end);
                    currentStepCounter++;
                } else if (currentStepCounter >= record.getMoveRecord().size()) {
                    Toast.makeText(PlayBack.this, win + " (" + way + ")",
                            Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    public void showStep(int[] start, int[] end) {

        this.chess.move(start, end, "normal");

        updateTextView();

        Piece target = chess.gameBoard.board[end[0]][end[1]];
        if (choice >= 0 && choice <= 3 && target != null) {
            chess.magicChange(target, items[choice]);
        }
        GridView xx = findViewById(R.id.gridViewPlayBack);
        ((BaseAdapter) xx.getAdapter()).notifyDataSetChanged();

    }
    public  void updateTextView(){
        if(chess.inAction.color.equals("white")){
            textViewBlack.setText("Black: wait");
            textViewWhite.setText("White: move");
        }else{
            textViewBlack.setText("Black: move");
            textViewWhite.setText("White: wait");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if ( id == android.R.id.home ) {
            startActivity(new Intent(PlayBack.this, RecordList.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onPause() {
        super.onPause();

        SharedPreferences chessApp = PreferenceManager
                .getDefaultSharedPreferences(this.getApplicationContext());
        if (chessApp != null) {

            SharedPreferences.Editor chessAppEditor = chessApp.edit();
            Gson gson = new Gson();
            String json = gson.toJson(MainActivity.getRecords());
            chessAppEditor.remove("records").apply();
            chessAppEditor.putString("records", json);
            chessAppEditor.apply();
        }

    }

//    public boolean dispatchKeyEvent(KeyEvent event) {
//        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK ) {
//            //do something.
//            return true;
//        } else {
//            return super.dispatchKeyEvent(event);
//        }
//    }
}
