package com.example.android77;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.android77.R;
import com.example.android77.chess.Record;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button newgameButton;
    Button recordedButton;


    private static ArrayList<Record> records = new ArrayList<>();
    private SharedPreferences chessApp;
    private static boolean FirstRun = true;


    public static ArrayList<Record> getRecords() {
        return records;
    }

    public static void addRecord(Record record) {
        records.add(record);
    }

    public static void setRecords(ArrayList<Record> records) {
        MainActivity.records = records;
    }

    public static void removeLastRecord() {
        records.remove(records.size() - 1);
    }


    @Override
    protected void onPause() {
        super.onPause();
        chessApp = PreferenceManager
                .getDefaultSharedPreferences(this.getApplicationContext());
        if (chessApp != null) {

            SharedPreferences.Editor chessAppEditor = chessApp.edit();
            Gson gson = new Gson();
            String json = gson.toJson(records);
            chessAppEditor.remove("records").apply();
            chessAppEditor.putString("records", json);
            chessAppEditor.apply();
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);
        if (FirstRun) {
            chessApp = PreferenceManager
                    .getDefaultSharedPreferences(this.getApplicationContext());
            Gson gson = new Gson();
            if (chessApp != null) {
                String json = chessApp.getString("records", "");
                if (json.isEmpty()) {
                    records = new ArrayList<>();
                } else {
                    Type type = new TypeToken<ArrayList<Record>>() {
                    }.getType();
                    records = gson.fromJson(json, type);
                }
            }
            FirstRun = false;
        }

        newgameButton = findViewById(R.id.newGameButton);
        newgameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Play.class));
            }
        });
        recordedButton = findViewById(R.id.recordedbutton);
        recordedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, RecordList.class));
            }
        });
    }
//    public boolean dispatchKeyEvent(KeyEvent event) {
//        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK ) {
//            //do something.
//            return true;
//        } else {
//            return super.dispatchKeyEvent(event);
//        }
//    }


}
