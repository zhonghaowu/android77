package com.example.android77;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.android77.R;
import com.example.android77.chess.Record;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;


public class RecordList extends AppCompatActivity {
    ArrayList<Record> recordList = MainActivity.getRecords();
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recorded_games);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            //do nothing;
        }

        recyclerView = findViewById(R.id.recyclerView);

        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter
        mAdapter = new MyAdapter(recordList);
        mAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(mAdapter);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_record_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case R.id.sortByTitleFromAtoZ:
                recordList.sort((o1, o2) -> o1.getTitle().compareTo(o2.getTitle()));
                mAdapter.notifyDataSetChanged();
                Toast.makeText(this, "Sort by Title from A to Z", Toast.LENGTH_SHORT).show();
                break;

            case R.id.sortByTitleFromZtoA:
                recordList.sort((o1, o2) -> o1.getTitle().compareTo(o2.getTitle()));
                Collections.reverse(recordList);
                mAdapter.notifyDataSetChanged();
                Toast.makeText(this, "Sort by Title from Z to A", Toast.LENGTH_SHORT).show();
                break;

            case R.id.sortByDateLatestFirst:
                recordList.sort((o1, o2) -> o1.getDate().compareTo(o2.getDate()));
                Collections.reverse(recordList);
                mAdapter.notifyDataSetChanged();
                Toast.makeText(this, "Sort by Date (latest first)", Toast.LENGTH_SHORT).show();
                break;

            case R.id.sortByDateEarliestFirst:
                recordList.sort((o1, o2) -> o1.getDate().compareTo(o2.getDate()));

                mAdapter.notifyDataSetChanged();
                Toast.makeText(this, "Sort by Date (earliest first) ", Toast.LENGTH_SHORT).show();
                break;

            case android.R.id.home:
                startActivity(new Intent(RecordList.this, MainActivity.class));
        }
        return true;
    }
    public void onPause() {
        super.onPause();

        SharedPreferences chessApp = PreferenceManager
                .getDefaultSharedPreferences(this.getApplicationContext());
        if (chessApp != null) {

            SharedPreferences.Editor chessAppEditor = chessApp.edit();
            Gson gson = new Gson();
            String json = gson.toJson(MainActivity.getRecords());
            chessAppEditor.remove("records").apply();
            chessAppEditor.putString("records", json);
            chessAppEditor.apply();
        }

    }

//    public boolean dispatchKeyEvent(KeyEvent event) {
//        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK ) {
//            //do something.
//            return true;
//        } else {
//            return super.dispatchKeyEvent(event);
//        }
//    }

}

