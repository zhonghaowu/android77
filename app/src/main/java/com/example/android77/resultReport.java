package com.example.android77;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android77.R;
import com.example.android77.chess.Record;

import java.util.ArrayList;
import java.util.Date;

public class resultReport extends AppCompatActivity {
    TextView textView;
    TextView textView3;
    Button button2;
    ArrayList<int[][]> moveRecord;
    Date date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_report);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle bundle = getIntent().getExtras();
        textView = findViewById(R.id.textView);
        textView3 = findViewById(R.id.textView3);
        button2 = findViewById(R.id.button2);
        if(bundle != null){
            textView.setText(bundle.getString("win"));
            textView3.setText(bundle.getString("way"));
            moveRecord = (ArrayList<int[][]>) bundle.getSerializable("moveRecord");
            date = (Date)bundle.getSerializable("date");
        }

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(resultReport.this);
                View view = getLayoutInflater().inflate(R.layout.activity_result_report_store, null);
                builder.setView(view);
                AlertDialog dialog = builder.create();
                dialog.show();
                final EditText reportName = view.findViewById(R.id.reportNameText);
                Button confirmButton = view.findViewById(R.id.confirmButton);
                Button cancelButton = view.findViewById(R.id.cancelButton);
                confirmButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ArrayList<String> titles = new ArrayList<>();
                        MainActivity.getRecords().forEach((n) -> titles.add(n.getTitle()));
                        if (!reportName.getText().toString().isEmpty()) {
                            if(titles.contains(reportName.getText().toString())) {
                                Toast.makeText(resultReport.this, "Please enter a different name",
                                        Toast.LENGTH_SHORT).show();
                                return;
                            } else {
                                MainActivity.removeLastRecord();
                                String title = reportName.getText().toString();
                                Record record = new Record(bundle.getString("win"),
                                        bundle.getString("way"), title, date, moveRecord);
                                MainActivity.addRecord(record);
                                Toast.makeText(resultReport.this, "Store successful",
                                        Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(resultReport.this, "Name is empty",
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }

                        finish();
                        dialog.dismiss();
                        startActivity(new Intent(resultReport.this, MainActivity.class));
                    }

                });

                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setResult(RESULT_CANCELED);
                        dialog.dismiss();
                    }
                });
                
            }
        });

    }

//    public boolean dispatchKeyEvent(KeyEvent event) {
//        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK ) {
//            //do something.
//            return true;
//        } else {
//            return super.dispatchKeyEvent(event);
//        }
//    }

}
