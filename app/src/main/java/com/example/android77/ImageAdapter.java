package com.example.android77;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.android77.chess.SharedStorage;

public class ImageAdapter extends BaseAdapter {
    private Context mContext;

    public ImageAdapter(Context c) {
        mContext = c;
    }

    public int getCount() {
        return SharedStorage.gridViewData.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {  // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(LayoutParams.MATCH_PARENT, 90));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            imageView = (ImageView) convertView;
        }
        imageView.setImageResource(SharedStorage.gridViewData[position]);
        int row = position / 8;
        if((position % 2 == 0 && row % 2 == 0) || (position % 2 == 1 && row % 2 == 1))imageView.setBackgroundColor(Color.parseColor("#EEEED2"));
        else imageView.setBackgroundColor(Color.parseColor("#769656"));
        if(SharedStorage.highlightPos == position){
            return imageView;
        }
        return imageView;
    }

    public Integer getImgID(int position){
        return SharedStorage.gridViewData[position];
    }



}