package com.example.android77;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android77.R;
import com.example.android77.chess.Chess;
import com.example.android77.chess.Record;
import com.example.android77.chess.SharedStorage;
import com.example.android77.piece.Piece;
import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Stack;

public class Play extends AppCompatActivity {
    static String state = "WAIT_STATE";
    static int[] temp;

    Button drawButton;
    Button aiButton;
    //int[0][0] == -1: resign here
    //int[0][0] == -2: draw here
    //int[2][0] == 0: normal move || promoted to queen
    //int[2][0] == 1: promote to rook
    //int[2][0] == 2: promote to bishop
    //int[2][0] == 3: promote to knight
    TextView textViewBlack;
    TextView textViewWhite;
    Stack<int[][]> move_record;
    Button resignButton;
    Button undoButton;
    Chess chess;
    String win = "unfinished";
    String way = "Exit halfway";
    Date currentDate = new Date();
    ArrayList<int[][]> moveRecord = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        move_record = new Stack<>();
        chess = new Chess();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        drawButton = findViewById(R.id.drawButton);
        aiButton = findViewById(R.id.aiButton);
        resignButton = findViewById(R.id.resignButton);
        undoButton = findViewById(R.id.undoButton);
        textViewBlack = findViewById(R.id.textView4);
        textViewWhite = findViewById(R.id.textView5);
        GridView gridView = (GridView)findViewById(R.id.gridview);
        ImageAdapter myImgAdapter = new ImageAdapter(this);
        gridView.setAdapter(myImgAdapter);
        updateTextView();
        aiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(chess.randomMove().equals("stalemate")){
                    Toast ts = Toast.makeText(getBaseContext(),"stalemate, cannot do AI move",Toast.LENGTH_SHORT);
                    ts.show();
                }else{
                    int[][] record = new int[3][];
                    record[0] = Arrays.copyOf(SharedStorage.random_start, 2);
                    record[1] = Arrays.copyOf(SharedStorage.random_end, 2);
                    record[2] = new int[2];
                    record[2][0] = -1;
                    move_record.push(record);
                    state = "WAIT_STATE";
                    SharedStorage.highlightPos = -1;
                    GridView gridViewx = findViewById(R.id.gridview);
                    if(temp != null) gridViewx.getChildAt(temp[0] * 8 + temp[1]).setBackgroundColor(Color.parseColor(Util.retCorrectGridColor(temp[0] * 8 + temp[1])));
                    temp = null;
                    updateTextView();
                    ((BaseAdapter) gridViewx.getAdapter()).notifyDataSetChanged();
                }
            }
        });
        undoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rollBack();
            }
        });
        drawButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                win = "DRAW";
                way = "Draw with consensus";
                goToResult();


            }
        });
        resignButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(chess.inAction.color.equals("black"))    win = "WHITE WIN";
                else    win = "BLACK WIN";
                way = "One player resigned";
                goToResult();

            }
        });
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                LayoutInflater inflater = getLayoutInflater();
                Log.d("clickGrid", position + "");
                int[] nextCor = Util.posToCor(position);
                if(state.equals("WAIT_STATE")){
                    if(SharedStorage.chessBoard[nextCor[0]][nextCor[1]] == null || !SharedStorage.chessBoard[nextCor[0]][nextCor[1]].color.equals(chess.inAction.color))   {}
                    else{
                        temp = nextCor;
                        state = "WAIT_DEST";
                        SharedStorage.highlightPos = position;
                        GridView gridViewx = findViewById(R.id.gridview);
                        gridViewx.getChildAt(position).setBackgroundColor(Color.parseColor("#EEE70A"));
                        ((BaseAdapter) gridViewx.getAdapter()).notifyDataSetChanged();
                    }
                }else if(state.equals("WAIT_DEST")){
                    String original_type = chess.gameBoard.board[temp[0]][temp[1]].type;
                    String feedback = chess.move(temp, nextCor, "normal");
                    Log.d("feedback",feedback);
                    if(feedback.equals("invalid")){
                        if(SharedStorage.chessBoard[nextCor[0]][nextCor[1]] != null && SharedStorage.chessBoard[nextCor[0]][nextCor[1]].color.equals(chess.inAction.color)){
                            temp = nextCor;
                            state = "WAIT_DEST";
                            SharedStorage.highlightPos = position;
                            GridView gridViewx = findViewById(R.id.gridview);
                            gridViewx.getChildAt(position).setBackgroundColor(Color.parseColor("#EEE70A"));
                            ((BaseAdapter) gridViewx.getAdapter()).notifyDataSetChanged();

                        }
                        Toast ts = Toast.makeText(getBaseContext(),"invalid move",Toast.LENGTH_SHORT);
                        ts.show();
                        return;
                    }else if(feedback.equals("valid")){
                        Piece pp = chess.gameBoard.board[nextCor[0]][nextCor[1]];
                        if(original_type.equals("pawn") &&
                                ((pp.color.equals("white") && nextCor[0] == 0) || (pp.color.equals("black") && nextCor[0] == 7))){
                            target = pp;
                            //give a dialog
                            choosePromote();
                        }
                    }else if(feedback.equals("check")){
                        Toast ts = Toast.makeText(getBaseContext(),"check",Toast.LENGTH_LONG);
                        ts.show();
                        Piece pp = chess.gameBoard.board[nextCor[0]][nextCor[1]];
                        if(original_type.equals("pawn") &&
                                ((pp.color.equals("white") && nextCor[0] == 0) || (pp.color.equals("black") && nextCor[0] == 7))){
                            target = pp;
                            //give a dialog
                            choosePromote();
                        }
                    }else if(feedback.equals("checkmate_black_win")){

                        win = "BLACK WIN";
                        way = "checkmate win";
                        goToResult();

                    }else if(feedback.equals("checkmate_white_win")){

                        win = "WHITE WIN";
                        way = "checkmate win";
                        goToResult();

                    }
                    int[][]record = new int[3][];
                    record[0] = Arrays.copyOf(temp, 2);
                    record[1] = Arrays.copyOf(nextCor, 2);
                    record[2] = new int[2];
                    record[2][0] = -1;
                    move_record.push(record);
                    state = "WAIT_STATE";
                    SharedStorage.highlightPos = -1;
                    GridView gridViewx = findViewById(R.id.gridview);
                    gridViewx.getChildAt(temp[0] * 8 + temp[1]).setBackgroundColor(Color.parseColor(Util.retCorrectGridColor(temp[0] * 8 + temp[1])));
                    temp = null;
                    ((BaseAdapter) gridViewx.getAdapter()).notifyDataSetChanged();
                    updateTextView();

                }
            }
        });

    }

//    public void goToResult(String winInfo) {
//        Bundle bundle = new Bundle();
//        bundle.putString("win",winInfo);
//        Intent intent = new Intent(this, resultReport.class);
//        intent.putExtras(bundle);
//
//        startActivity(intent);
//        //12345means transfer from chess to result
//    }
    public void goToResult() {
        Intent intent = new Intent(Play.this, resultReport.class);
        Bundle info = new Bundle();
        info.putString("win", win);
        info.putString("way", way);
        currentDate = Calendar.getInstance().getTime();
        moveRecord = new ArrayList(move_record);
        info.putSerializable("date", currentDate);
        info.putSerializable("moveRecord", moveRecord);
        intent.putExtras(info);
        startActivity(intent);
        finish();
    }

    public void onPause() {
        super.onPause();
        currentDate = Calendar.getInstance().getTime();
        moveRecord = new ArrayList(move_record);
        DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
        String strTime = dateFormat.format(currentDate);
        Record record = new Record(win, way,
                "-AutoSave Record " + strTime, currentDate, moveRecord);
        MainActivity.addRecord(record);

        SharedPreferences chessApp = PreferenceManager
                .getDefaultSharedPreferences(this.getApplicationContext());
        if (chessApp != null) {

            SharedPreferences.Editor chessAppEditor = chessApp.edit();
            Gson gson = new Gson();
            String json = gson.toJson(MainActivity.getRecords());
            chessAppEditor.remove("records").apply();
            chessAppEditor.putString("records", json);
            chessAppEditor.apply();
        }

    }

    static int choice = -1;
    static Piece target;
    final String[] items = {"queen", "rook", "bishop", "knight"};
    static int promotion = 0;
    private void choosePromote() {
        promotion = 0;
        AlertDialog.Builder builder = new AlertDialog.Builder(this).setIcon(R.mipmap.ic_launcher).setTitle("promotion")
                .setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        choice = i;
                        promotion = choice;
                    }
                }).setPositiveButton("confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        move_record.peek()[2][0] = choice;
                        Log.d("choice", choice +"");
                        if (choice != -1 || choice != 0) {
                            chess.magicChange(target, items[choice]);
                        }else{
                            chess.magicChange(target, "queen");
                        }
                        GridView xx = findViewById(R.id.gridview);
                        ((BaseAdapter) xx.getAdapter()).notifyDataSetChanged();
                    }
                });
        builder.create().show();
        choice = 0;
    }

    public void rollBack() {
         if(chess.lastChess == null)  {
             Toast ts = Toast.makeText(getBaseContext(),"you cannot undo now",Toast.LENGTH_SHORT);
             ts.show();
             return;
         }
         this.chess = chess.lastChess;
         state = "WAIT_STATE";
         temp = null;
         chess.lastChess = null;
         if(textViewWhite != null){updateTextView();}
         SharedStorage.chessBoard = this.chess.gameBoard.board;
         SharedStorage.updateImage(this.chess.gameBoard.board);
         GridView xx = findViewById(R.id.gridview);
         ((BaseAdapter) xx.getAdapter()).notifyDataSetChanged();
         move_record.pop();
    }

    public  void updateTextView(){
        if(chess.inAction.color.equals("white")){
            textViewBlack.setText("Black: wait");
            textViewWhite.setText("White: move");
        }else{
            textViewBlack.setText("Black: move");
            textViewWhite.setText("White: wait");
        }
    }
//    public boolean dispatchKeyEvent(KeyEvent event) {
//        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK ) {
//            //do something.
//            return true;
//        } else {
//            return super.dispatchKeyEvent(event);
//        }
//    }

}
