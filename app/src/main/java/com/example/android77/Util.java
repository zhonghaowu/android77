package com.example.android77;

public class Util {
    public static int[] posToCor(int pos){
        int[] ret = new int[2];
        ret[0] = pos/8;
        ret[1] = pos%8;
        return ret;
    }
    public static int corToPos(int[] cor){
        int ret;
        ret = cor[0]*8 + cor[1];
        return ret;
    }

    public static String retCorrectGridColor(int position){
        int row = position / 8;
        if((position % 2 == 0 && row % 2 == 0) || (position % 2 == 1 && row % 2 == 1))  return "#EEEED2";
        else return "#769656";
    }
}
